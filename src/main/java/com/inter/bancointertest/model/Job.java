package com.inter.bancointertest.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Job implements Comparable<Job>{
	

	@Id
	private Integer id;
	
	private String name;
	
	private Boolean active;
	
	@OneToMany(mappedBy = "job")
	@JsonManagedReference
	private List<Task> tasks;

	@ManyToOne
	@JoinColumn(name = "parent_Job_id")
	private Job parentJob;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public Job getParentJob() {
		return parentJob;
	}

	public void setParentJob(Job parentJob) {
		this.parentJob = parentJob;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}
	
	@JsonIgnore
	public Integer getWeightOfTasks() {
		Integer sum = 0;
		if(tasks != null) {
			for(Task task : tasks) {
				sum += task.getWeight();
			}
		}
		return sum;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Job other = (Job) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
    public int compareTo(Job other) {
        if (this.getWeightOfTasks() < other.getWeightOfTasks()) {
            return -1;
        }
        if (this.getWeightOfTasks() == other.getWeightOfTasks()) {
            return 0;
        }
        return 1;
    }
	
	
	
	
}
