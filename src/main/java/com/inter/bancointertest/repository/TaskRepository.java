package com.inter.bancointertest.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.inter.bancointertest.model.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task, Integer> {
	
	public List<Task> findByCreatedAt(Date date);
	
}
