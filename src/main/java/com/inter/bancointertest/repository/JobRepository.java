package com.inter.bancointertest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.inter.bancointertest.model.Job;

@Repository
public interface JobRepository extends JpaRepository<Job, Integer> {

}
