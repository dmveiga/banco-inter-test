package com.inter.bancointertest.controller;

import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.inter.bancointertest.model.Job;
import com.inter.bancointertest.repository.JobRepository;
import com.inter.bancointertest.service.JobService;

//TODO Response Status for all requests
@RestController
public class JobController {

	@Autowired
	private JobRepository jobRepo;
	
	@Autowired
	private JobService jobService;
	
	@GetMapping("/jobs")
	public List<Job> retrieveAllJobs(
			@RequestParam(value = "sortByWeight", required = false, defaultValue = "false") Boolean sortByWeight) {
		
		List<Job> jobs = jobRepo.findAll();
		
		if(sortByWeight){
			Collections.sort(jobs, Collections.reverseOrder());
		}
		
		return jobs;
	}
	
	@GetMapping("/jobs/{id}")
	public Job retrieveJob(@PathVariable Integer id) {
		
		Optional<Job> job= jobRepo.findById(id);
		if(job.isPresent()) {
			return job.get();
		}
		return null;
	}
	
	@DeleteMapping("/jobs/{id}")
	public void deleteJob(@PathVariable Integer id){
		jobRepo.deleteById(id);
	}
	
	@PostMapping("/jobs")
	public ResponseEntity<Object> createJob(@RequestBody Job job) {
		Job savedJob = jobRepo.save(job);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedJob.getId()).toUri();
		
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping("/jobs/{id}")
	public ResponseEntity<Object> updateJob(@RequestBody Job job, @PathVariable Integer id){
		
		if(!jobService.validateParentJob(job)) {
			return ResponseEntity.noContent().build();
		}
		
		Optional<Job> jobOptional = jobRepo.findById(id);
		
		if(!jobOptional.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		job.setId(id);
		
		Job savedJob = jobRepo.save(job);
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedJob.getId()).toUri();
		
		return ResponseEntity.created(location).build();
	}
	
	
}
