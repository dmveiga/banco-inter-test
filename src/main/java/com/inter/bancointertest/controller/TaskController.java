package com.inter.bancointertest.controller;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.inter.bancointertest.model.Task;
import com.inter.bancointertest.repository.TaskRepository;

@RestController
public class TaskController {
	
	@Autowired
	private TaskRepository taskRepo;
	
	@GetMapping("/tasks")
	public List<Task> retrieveAllTasks(
			@RequestParam(value = "createdAt", required = false, defaultValue = "") @DateTimeFormat(pattern="yyyy-MM-dd") Date createdAt){
		if(createdAt != null) {
			return taskRepo.findByCreatedAt(createdAt);
		}
		return taskRepo.findAll();
	}
	
	@GetMapping("/tasks/{id}")
	public Task retrieveTask(@PathVariable Integer id) {
		
		Optional<Task> task= taskRepo.findById(id);
		if(task.isPresent()) {
			return task.get();
		}
		return null;
	}
	
	@DeleteMapping("/tasks/{id}")
	public void deleteTask(@PathVariable Integer id){
		taskRepo.deleteById(id);
	}
	
	@PostMapping("/tasks")
	public ResponseEntity<Object> createTask(@RequestBody Task task) {
		Task savedTask = taskRepo.save(task);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedTask.getId()).toUri();
		
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping("/tasks/{id}")
	public ResponseEntity<Object> updateTask(@RequestBody Task task, @PathVariable Integer id){
		
		Optional<Task> taskOptional = taskRepo.findById(id);
		
		if(!taskOptional.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		task.setId(id);
		
		taskRepo.save(task);
		
		return ResponseEntity.noContent().build();
	}

}
