package com.inter.bancointertest.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inter.bancointertest.model.Job;
import com.inter.bancointertest.repository.JobRepository;

@Service
public class JobService {
	
	@Autowired
	private JobRepository jobRepo;
	
	//TODO Make this code better
	/**
	 * Of course, there are better ways to validade this, but I neither had enought time to think.
	 * It´s working, so for now I will focus in get things done before fixing this
	 * 
	 * @param job
	 * @return if this job has itself in its tree node above
	 */
	public boolean validateParentJob(Job job) {
		
		Job originalJob = job;
		
		if(job.getParentJob() != null && job.equals(job.getParentJob())) {
			return false;
		}
		
		Optional<Job> jobOptional = jobRepo.findById(job.getParentJob().getId());
		job = jobOptional.get();
		
		while(job.getParentJob() != null && !originalJob.equals(job)) {
			Optional<Job> jobParentOptional = jobRepo.findById(job.getParentJob().getId());
			if(jobParentOptional.get().equals(originalJob)) {
				return false;
			}
			job = jobParentOptional.get();
		}
		
		return true;
	}

}
